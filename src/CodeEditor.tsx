import React, {
    useEffect,
    useContext,
    useRef,
    createRef,
    useLayoutEffect,
    useState,
  } from "react";
  import MonacoEditor, { monaco } from "@monaco-editor/react";
  import language from "react-syntax-highlighter/dist/esm/languages/hljs/1c";
  
  interface ICodeEditorProps {
    height: string;
    initValue: string;
    language: "python" | "json" | "javascript";
    onChange: (code: string) => void;
    // formatOnLoad?: boolean | false;
    shouldEditorFormat?: boolean | false;
  }
  
  const CodeEditor: React.FC<ICodeEditorProps> = (props) => {
    const [monacoInstance, setMonacoInstance] = useState<any>();
    const valueGetter = useRef<any>(undefined);
    const [isEditorReady, setIsEditorReady] = useState(false);
    const [codeState, setCodeState] = useState(props.initValue);
    const editorRef = useRef<any>();
  
    useEffect(() => {
      document.addEventListener("keyup", saveCodeState, false);
    }, []);
    useEffect(() => {
      document.addEventListener("keyup", saveCodeState, false);
      return () => {
        document.removeEventListener("keyup", saveCodeState, false);
      };
    }, [valueGetter, isEditorReady]);
  
    useEffect(() => {
      if (isEditorReady && monacoInstance !== undefined) {
          monacoInstance.editor.setTheme("monokai");
      }
    }, [isEditorReady, monacoInstance]);
  
    useEffect(() => {
      if (props.shouldEditorFormat === true && editorRef !== undefined) {
        if (editorRef.current !== undefined) {
          editorRef!.current?.getAction("editor.action.formatDocument").run();
        }
      }
    }, [props.shouldEditorFormat]);
  
    function saveCodeState() {
      let latest = getLatestCode();
      setCodeState(latest);
      props.onChange(latest);
    }
  
    function handleEditorDidMount(_valueGetter: any, editor: any) {
      editorRef.current = editor;
      if (props.shouldEditorFormat) {
        setTimeout(() => {
          editor.getAction("editor.action.formatDocument").run();
        }, 350);
      }
      monaco
        .init()
        .then((monaco) => {
          setMonacoInstance(monaco);
          import("./Monokai.json").then((data) => {
            monaco.editor.defineTheme("monokai", data);
              monaco.editor.setTheme("monokai");
          });
        })
        .catch((error) =>
          console.error(
            "An error occurred during initialization of Monaco: ",
            error
          )
        );
  
      valueGetter.current = _valueGetter;
      setIsEditorReady(true);
    }
  
    function getLatestCode() {
      if (isEditorReady && valueGetter !== undefined) {
        if (valueGetter.current != undefined) {
          return valueGetter.current();
        }
      }
      return "";
    }
  
    return (
      <MonacoEditor
        height={props.height}
        value={props.initValue}
        theme={"monokai"}
        language={props.language}
        editorDidMount={handleEditorDidMount}
        options={editorOptions}
      />
    );
  };
  
  export default CodeEditor;
  
  const editorOptions = {
    acceptSuggestionOnCommitCharacter: true,
    acceptSuggestionOnEnter: "on",
    accessibilitySupport: "auto",
    autoIndent: false,
    automaticLayout: true,
    codeLens: true,
    colorDecorators: true,
    contextmenu: true,
    cursorBlinking: "blink",
    cursorSmoothCaretAnimation: false,
    cursorStyle: "line",
    disableLayerHinting: false,
    disableMonospaceOptimizations: false,
    dragAndDrop: false,
    fixedOverflowWidgets: false,
    folding: true,
    foldingStrategy: "auto",
    fontLigatures: false,
    formatOnPaste: false,
    formatOnType: false,
    hideCursorInOverviewRuler: false,
    highlightActiveIndentGuide: true,
    links: true,
    mouseWheelZoom: false,
    multiCursorMergeOverlapping: true,
    multiCursorModifier: "alt",
    overviewRulerBorder: true,
    overviewRulerLanes: 2,
    quickSuggestions: true,
    quickSuggestionsDelay: 100,
    readOnly: false,
    renderControlCharacters: false,
    renderFinalNewline: true,
    renderIndentGuides: true,
    renderLineHighlight: "all",
    renderWhitespace: "none",
    revealHorizontalRightPadding: 30,
    roundedSelection: true,
    rulers: [],
    scrollbar: {
      alwaysConsumeMouseWheel: false,
    },
    scrollBeyondLastColumn: 5,
    scrollBeyondLastLine: true,
    selectOnLineNumbers: true,
    selectionClipboard: true,
    selectionHighlight: true,
    showFoldingControls: "mouseover",
    smoothScrolling: false,
    suggestOnTriggerCharacters: true,
    wordBasedSuggestions: true,
    wordSeparators: "~!@#$%^&*()-=+[{]}|;:'\",.<>/?",
    wordWrap: "off",
    wordWrapBreakAfterCharacters: "\t})]?|&,;",
    wordWrapBreakBeforeCharacters: "{([+",
    wordWrapBreakObtrusiveCharacters: ".",
    wordWrapColumn: 80,
    wordWrapMinified: true,
    wrappingIndent: "none",
  };
  