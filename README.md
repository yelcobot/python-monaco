A basic code editor.

# The Goal: Add python language support

There are 3 main pieces to this:

1. Get a PYLS running
2. Get the monaco client
3. A proxy server to wrap the PYLS
